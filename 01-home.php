<?php
/**
 * Template Name: 01 - Home
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/html-header', 'parts/header' ) ); ?>

<div class="homepage">

	<div class="home-header module">

		<iframe class="desktop" src="https://player.vimeo.com/video/<?php echo the_field('homepage_desktop_video_id', 'options'); ?>?background=1" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		<iframe class="mobile" src="https://player.vimeo.com/video/<?php echo the_field('homepage_mobile_video_id', 'options'); ?>?background=1" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<div class="home-header-inner">
			<div class="container">
				<?php $post_object = get_field('featured_service'); ?>
				<?php if( $post_object ):  ?>
					<?php $post = $post_object; ?>
					<?php setup_postdata( $post );  ?>
					<!-- <div> -->
						<h3>Featured Service</h3>
						<h2><?php the_title(); ?></h2>
						<a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>">View Service</a>
					<!-- </div> -->
					<?php unset($post_object, $post); ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
		</div>

		<div class="scroll"></div>

	</div>

	<div class="service-row one module">

		<div class="container clearfix">
			<?php $serviceRow1 = get_field('services_row_one'); ?>
			<?php $post_object = $serviceRow1['service_one']; ?>

			<?php if( $post_object ): ?>
				<?php $post = $post_object; ?>
				<?php setup_postdata( $post ); ?>
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<a href="<?php the_permalink(); ?>" class="service one">
					<div class="background" style="background-image: url('<?php echo $url; ?>');"></div>
					<div class="content">
						<h3><?php the_title(); ?></h3>
						<p><?php the_field('read_more_text'); ?></p>
					</div>
				</a>
				<?php unset($post_object, $post); ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

			<?php $serviceRow1 = get_field('services_row_one'); ?>
			<?php $post_object = $serviceRow1['service_two']; ?>

			<?php if( $post_object ): ?>
				<?php $post = $post_object; ?>
				<?php setup_postdata( $post ); ?>
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<a href="<?php the_permalink(); ?>" class="service two">
					<div class="background" style="background-image: url('<?php echo $url; ?>');"></div>
					<div class="content">
						<h3><?php the_title(); ?></h3>
						<p><?php the_field('read_more_text'); ?></p>
					</div>
				</a>
				<?php unset($post_object, $post); ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

			<div class="view-all-container">
				<?php $serviceRowOneButton = get_field('service_row_one_button'); ?>
				<?php if($serviceRowOneButton): ?>
					<a href="<?php echo $serviceRowOneButton['url']; ?>" class="view-all"><?php echo $serviceRowOneButton['title']; ?></a>
				<?php else: ?>
					<a href="<?php echo get_home_url(); ?>/services" class="view-all">View All Services</a>
				<?php endif; ?>
			</div>

		</div>

	</div>

	<div class="home-text-row module">
		<div class="container">
			<p><?php the_field('text_row'); ?></p>
		</div>
	</div>

	<div class="service-row two module">

		<div class="container clearfix">
			<?php $serviceRow1 = get_field('services_row_two'); ?>
			<?php $post_object = $serviceRow1['service_one']; ?>

			<?php if( $post_object ): ?>
				<?php $post = $post_object; ?>
				<?php setup_postdata( $post ); ?>
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<a href="<?php the_permalink(); ?>" class="service one">
					<div class="background" style="background-image: url('<?php echo $url; ?>');"></div>
					<div class="content">
						<h3><?php the_title(); ?></h3>
						<p><?php the_field('read_more_text'); ?></p>
					</div>
				</a>
				<?php unset($post_object, $post); ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

			<?php $serviceRow1 = get_field('services_row_two'); ?>
			<?php $post_object = $serviceRow1['service_two']; ?>

			<?php if( $post_object ): ?>
				<?php $post = $post_object; ?>
				<?php setup_postdata( $post ); ?>
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<a href="<?php the_permalink(); ?>" class="service two">
					<div class="background" style="background-image: url('<?php echo $url; ?>');"></div>
					<div class="content">
						<h3><?php the_title(); ?></h3>
						<p><?php the_field('read_more_text'); ?></p>
					</div>
				</a>
				<?php unset($post_object, $post); ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

			<div class="view-all-container">
				<?php $serviceRowTwoButton = get_field('service_row_two_button'); ?>
				<?php if($serviceRowTwoButton): ?>
					<a href="<?php echo $serviceRowTwoButton['url']; ?>" class="view-all"><?php echo $serviceRowTwoButton['title']; ?></a>
				<?php else: ?>
					<a href="<?php echo get_home_url(); ?>/services" class="view-all">View All Services</a>
				<?php endif; ?>
			</div>

		</div>

	</div>

</div>

<?php Starkers_Utilities::get_template_parts( array( 'parts/footer','parts/html-footer' ) ); ?>