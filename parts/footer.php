<footer>
	<div class="container clearfix">
		<p class="copy">&copy; Two Idiots & A Camera 2019 </p>
		<ul class="social">
			<?php $social = get_field('social', 'options'); ?>
			<?php if( $social['facebook'] ): ?>
				<li><a href="<?php echo $social['facebook']; ?>"><i class="fab fa-facebook"></i></a></li>
			<?php endif; ?>
			<?php if( $social['twitter'] ): ?>
				<li><a href="<?php echo $social['twitter']; ?>"><i class="fab fa-twitter"></i></a></li>
			<?php endif; ?>
			<?php if( $social['instagram'] ): ?>
				<li><a href="<?php echo $social['instagram']; ?>"><i class="fab fa-instagram"></i></a></li>
			<?php endif; ?>
			<?php if( $social['vimeo'] ): ?>
				<li><a href="<?php echo $social['vimeo']; ?>"><i class="fab fa-vimeo-v"></i></a></li>
			<?php endif; ?>
		</ul>
		<a class="tmwlsh" target="_blank" href="http://www.tmwlsh.co.uk">By Tmwlsh.</a>
	</div>
</footer>