<?php $logo = get_field('logo', 'options'); ?>

<div class="mobile-nav">
	<div class="container">
		<nav><?php wp_nav_menu( array( 'container_class' => 'primary-nav', 'theme_location' => 'header-menu' ) ); ?></nav>
	</div>
</div>

<header>
	<div class="container clearfix">
		<a href="<?php echo get_home_url(); ?>" alt="Home Link"><img src="<?php echo $logo['logo_image']; ?>" alt="Logo" /></a>
		<div class="hamb">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
		</div>
		<nav><?php wp_nav_menu( array( 'container_class' => 'primary-nav', 'theme_location' => 'header-menu' ) ); ?></nav>
	</div>
</header>