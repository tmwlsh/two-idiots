<?php /* Module: Full Width Image */ ?>

<div class="module module-full-width-image">
	<div class="container">
        <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('alt_text'); ?>" />
	</div>
</div>