<?php /* Module: Video List */ ?>

<div class="module module-video-list">
	<div class="container clearfix">
        <?php $title = get_sub_field('video_list_title'); ?>
        <?php if($title): ?>
            <h2><?php echo $title; ?></h2>
        <?php endif; ?>
        <?php if( have_rows('videos') ): ?>
            <?php while ( have_rows('videos') ) : the_row(); ?>
                <div class="video-container">
                    <iframe src="https://player.vimeo.com/video/<?php the_sub_field('video_id'); ?>?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php $social = get_field('social', 'options'); ?>
        <?php $showMore = get_sub_field('show_view_more'); ?>
        <?php if($showMore): ?>
            <h3>For more recent work, visit our <a href="<?php echo $social['vimeo']; ?>">Vimeo Page</a></h3>
        <?php endif; ?>
	</div>
</div>