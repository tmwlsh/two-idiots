<?php /* Module: Video Block */ ?>


<div class="module module-video-block">
	<div class="container">
		<iframe src="https://player.vimeo.com/video/<?php the_sub_field('video_id'); ?>?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>
</div>