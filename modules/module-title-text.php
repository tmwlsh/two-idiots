<?php /* Module: Title & Text */ ?>

<div class="module module-title-text">
	<div class="container clearfix">
        <div class="half">
            <h3><?php the_sub_field('title'); ?></h3>
        </div>
        <div class="half">
            <?php the_sub_field('text'); ?>
        </div>
	</div>
</div>