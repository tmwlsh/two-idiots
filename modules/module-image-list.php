<?php /* Module: Image List */ ?>

<div class="module module-image-list">
	<div class="container clearfix">
        <?php $title = get_sub_field('image_list_title'); ?>
        <?php if($title): ?>
            <h2><?php echo $title; ?></h2>
        <?php endif; ?>
        <?php if( have_rows('images') ): ?>
            <?php while ( have_rows('images') ) : the_row(); ?>
                <div class="image-container">
                    <img src="<?php the_sub_field('image'); ?>" />
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
	</div>
</div>