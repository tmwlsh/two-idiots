<?php /* Module: Page Title */ ?>
                            
<div class="module module-page-title">
	<div class="container">
		<h1><?php the_sub_field('title_text'); ?></h1>
	</div>
</div>