<?php
    /* Module: Contact Form */
    $contact = get_field('contact', 'options');

?>


<div class="module module-contact-form">
	<div class="container">
        <?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
        <ul class="contact">
            <?php if( $contact['phone_number'] ): ?>
                <li><a href="tel:<?php echo $contact['phone_number']; ?>"><span>T:</span> <?php echo $contact['phone_number']; ?></a></li>
            <?php endif; ?>
            <?php if( $contact['email_address'] ): ?>
                <li><a href="mailto:<?php echo $contact['email_address']; ?>"><span>E:</span> <?php echo $contact['email_address']; ?></a></li>
            <?php endif; ?>
            <?php if( $contact['address'] ): ?>
                <li><?php echo $contact['address']; ?></li>
            <?php endif; ?>
        </ul>
	</div>
</div>