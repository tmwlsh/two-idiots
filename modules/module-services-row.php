<?php /* Module: Services Row */ ?>

<div class="module module-services-row">
	<div class="container clearfix">
        <h2><?php the_sub_field('services_row_title'); ?></h2>

            <?php if( have_rows('service_repeater') ): ?>
                <?php while ( have_rows('service_repeater') ) : the_row(); ?>

                    <?php $post_object = get_sub_field('service'); ?>

                    <?php if( $post_object ): ?>
                        <?php $post = $post_object; ?>
                        <?php setup_postdata( $post ); ?>
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <a href="<?php the_permalink(); ?>" class="service">
                            <?php $offer = get_sub_field('offer'); ?>
                            <?php if($offer): ?>
                                <div class="offer"><?php echo $offer; ?></div>
                            <?php endif; ?>
                            <div class="background" style="background-image: url('<?php echo $url; ?>');">
                            </div>
                            <div class="content">

                                <h3><?php the_title(); ?></h3>
                                <p><?php the_field('read_more_text'); ?></p>
                            </div>
                        </a>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>

                <?php endwhile; ?>
            <?php endif; ?>

	</div>
</div>