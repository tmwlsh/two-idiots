<?php /* Module: Content builder */

$acf_id = mbl_acf_id();
if( have_rows('content_builder_modules', $acf_id) ): ?>

	<section class="content-builder">
		<?php while ( have_rows('content_builder_modules', $acf_id) ) : the_row();

			if( get_row_layout() == 'page_title' ) {
				get_template_part( 'modules/module', 'page-title' );
			} elseif( get_row_layout() == 'services_row' ) {
				get_template_part( 'modules/module', 'services-row' );
			} elseif( get_row_layout() == 'title_text' ) {
				get_template_part( 'modules/module', 'title-text' );
			} elseif( get_row_layout() == 'full_width_image' ) {
				get_template_part( 'modules/module', 'full-width-image' );
			} elseif( get_row_layout() == 'video_block' ) {
				get_template_part( 'modules/module', 'video-block' );
			} elseif( get_row_layout() == 'video_list' ) {
				get_template_part( 'modules/module', 'video-list' );
			} elseif( get_row_layout() == 'image_list' ) {
				get_template_part( 'modules/module', 'image-list' );
			} elseif( get_row_layout() == 'quote' ) {
				get_template_part( 'modules/module', 'quote' );
			} elseif( get_row_layout() == 'contact_form' ) {
				get_template_part( 'modules/module', 'contact-form' );
			}

		endwhile; ?><!-- .content-builder -->
	</section>

<?php endif;
