<?php /* Module: quote */ ?>

<div class="module module-quote">
	<div class="container">
        <span class="mark"><i class="fas fa-quote-right"></i></span>
        <h3><?php the_sub_field('quote_text'); ?></h3>
        <p>- <?php the_sub_field('quote_author'); ?></p>
	</div>
</div>