(function ($) {

	$(document).ready(function(){

        $(".module-video-block").fitVids();
        $(".module-video-list .video-container").fitVids();

        var controller = new ScrollMagic.Controller();

        // Loop through modules
        $(".module").each(function(){
            // build scene
            var ourScene = new ScrollMagic.Scene({
                triggerElement: this.children[0],
                triggerHook: 0.8,
                reverse: false,
            })
            .setClassToggle(this, "fade-in") // add class to module
            // .addIndicators()
            .addTo(controller);

        });


        $("div.hamb").click(function(){
            $(this).toggleClass("open");
            $("div.mobile-nav").toggleClass("open");
        });


        function pagePadding(){
            var $headerHeight = $("header").outerHeight();
            $("body.page-template-default").css("padding-top", $headerHeight);
            $("body.single-services").css("padding-top", $headerHeight);
        };

        function headerFade(){
            if ($(document).scrollTop() > 100) {
                $('header').addClass('scrolled');
            } else {
                $('header').removeClass('scrolled');
            }
        };

        pagePadding();

        $(window).scroll(function() {
            headerFade();
            pagePadding();
        });

    });

}(jQuery));